from grades.models import Grades
from django.db import models
import json
from django.core import serializers
from django.http import HttpResponse, HttpRequest


def StudentInfo(request: HttpRequest):
    if request.method == 'GET':
        name = request.GET.get('name', '')
        if not name:
            return HttpResponse("Student Grades")

        studentInfo = Grades.objects.get(name=name)
        #Change query to json format
        data = serializers.serialize("json", [studentInfo])
        #Change object to json object
        struct = json.loads(data)
        #Convert Python object into a string
        data = json.dumps(struct[0])
        response = HttpResponse(data, content_type="application/json")
        #return render(request, "StudentGrades/index.html", {"name": studentInfo.grade1})
        return response

    if request.method in ('POST', 'PUT'):
        data = request.body.decode('utf-8')
        struct = json.loads(data)
        try:
            student = Grades.objects.get(name=struct["name"])
        except:     
            Grades(name=struct["name"], grade1=struct["grade1"], grade2=struct["grade2"], grade3=struct["grade3"]).save()
            return HttpResponse("Post a new student successful")
        else:
            student.grade1 = struct["grade1"]
            student.grade2 = struct["grade2"]
            student.grade3 = struct["grade3"]
            student.save()
            return HttpResponse("Update students grade successful")
            
    elif request.method == 'DELETE':
        student.delete()
        return HttpResponse("Delete successfully")

def AverageGrade(request: HttpRequest):
    if request.method == 'GET':
        name = request.GET.get('name', '')
        if not name:
            return HttpResponse("Student Average Grades")

        studentInfo = Grades.objects.get(name=name)
        
    